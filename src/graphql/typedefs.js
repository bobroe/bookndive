module.exports.typeDefs = `
type Query {
  service(id: ID!): [Service],
  services(tags: [String]!): [Service]
  offers(tags: [String]!, maxprice: Float): [Service]
},
type Mutation {
  addService(title: String!): Service
  editService(id: ID!, title: String!): Service
  delService(id: ID!): String
},
type Provider {
  id: String
  title: String
  contact: [Contact]
  services: [Service]
  address: [Address]
  masters: [Master]
  facilities: String
  equipment: String
  decompressionDistance: Int
},
type Service {
  id: String
  title: String
  summary: String
  shortDescription: String
  extendedDescription: String
  images: [Image]
  requiredDocuments: String
  generalAdvice: String
  offers: [Offer]
  tags: [Tag]
},
type Offer {
  id: String
  title: String
  price: Float
  limit: Int
},
type Contact {
  primaryEmail: String
  secondaryEmail: String
  txtMessaging: String
  primaryMobile: String
  secondaryMobile: String
},
type Master {
  id: String
  fullName: String
  occupation: String
  images: [Image]
},
type Address {
  address: String
  city: String
  state: String
  zip: String
  country: String
},
type Image {
  id: String
  url: String
  title: String
},
type Tag {
  id: String
  title: String
}`;
