const { makeExecutableSchema } = require('graphql-tools');
const uuid = require('uuid');
const { find, filter } = require('lodash');
const GraphQL = require('graphql');
const {
  GraphQLList,
	GraphQLString,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema,
} = GraphQL;

const { typeDefs } = require('./typedefs');
const { mockdata } = require('./mockdata');
const service = require('../dynamo/service');
const filters = require('../search/filters.js');

const MAXPRICE = 1000000;

//Resolvers part
const resolvers = {
  Mutation: {
    addService: (parent,args) => {
      args.id = uuid.v1();
      return service.addService(args);
    },
    editService: (parent,args) => {
      return service.editService(args.id,args.title);
    },
    delService: (parent,args) => {
      return service.delService(args.id);
    },
  },
  Query: {
    service: (parent,args) => {
        return args.id
          ? [find(mockdata.service, {id: args.id})]
          : null;
    },
    services: (parent,args) => {
        return args.tags
          ? filters.findbytag(mockdata.service, args.tags)
          : null
    },
    offers: (parent,args) => {
      var maxprice = (args.maxprice)?args.maxprice:MAXPRICE;
        return args.tags
          ? filters.deepforeachbytag(mockdata.provider, args.tags, maxprice)
          : null
    }
  }
};

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = schema;
