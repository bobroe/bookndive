module.exports.mockdata = {
  service: [
    {
      id: "1",
      title: "Diving Thai",
      tags: [
        {id: "1",
        title: "Thailand"}
      ]
    },
    {
      id: "2",
      title: "Snorkeling Bali",
      tags: [
        {id: "2",
        title: "Bali"}
      ]
    },
    {
      id: "3",
      title: "Diving Vopasana",
      tags: [
        {id: "2",
        title: "Bali"}
      ]
    }
  ],
  provider: [
    {
      id: "45FF-UTR76",
      title: "Thai Diving Club",
      services: [
        {
          id: "45FF-UTR76-FTY1",
          title: "Snorkeling",
          tags: [
            {
              title: "Snorkeling"
            }
          ],
          offers: [
            {
              id: "45FF-FTY1",
              price: 100.0
            },
            {
              id: "45FF-FTY2",
              price: 200.0
            }
          ]
        },
        {
          id: "45FF-UTR76-FTY2",
          title: "Snorkeling 2",
          tags: [
            {
              title: "Diving"
            }
          ],
          offers: [
            {
              id: "45FF-FTY45",
              price: 100.0
            },
            {
              id: "45FF-FTY2",
              price: 200.0
            }
          ]
        }
      ]
    },
    {
      id: "45FF-UTR00",
      title: "Bali Diving Club",
      services: [
        {
          id: "45FF-UTR76-FTO9",
          title: "Snorkeling 55",
          tags: [
            {
              title: "Snorkeling"
            }
          ],
          offers: [
            {
              id: "45FF-FTZZ",
              price: 104.0
            },
            {
              id: "45FF-FTZZ",
              price: 209.0
            }
          ]
        },
        {
          id: "45FF-UTR76-FTWR",
          title: "Snorkeling 5",
          tags: [
            {
              title: "Snorkeling"
            }
          ],
          offers: [
            {
              id: "45FF-FTZZ",
              price: 190.0
            },
            {
              id: "45FF-FTY2",
              price: 290.0
            }
          ]
        }
      ]
    }
  ]
};
