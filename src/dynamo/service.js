const utils = require('./utils');
const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const delService = serviceId => utils.promisify(callback =>
  dynamoDb.delete({
    TableName: "Service",
    Key: { id: serviceId.toString() }
  }, callback))
  .then(result => {
    return serviceId
  });

const getService = serviceId => utils.promisify(callback =>
  dynamoDb.get({
    TableName: "Service",
    Key: { id: serviceId.toString() },
  }, callback))
  .then(result => {
    if(!result.Item) {
      return {error:"No data"}
    }
    return [result.Item]
  });

const addService = (objService) => utils.promisify(callback =>
  dynamoDb.put({
    TableName: "Service",
    Item: objService
  }, callback))
  .then(result => {
    //console.log(JSON.stringify(objCompany));
    return objService
  });

const editService = (serviceId, serviceTitle) => utils.promisify(callback =>
  dynamoDb.update({
    TableName: "Service",
    Key: { id: serviceId.toString() },
    UpdateExpression: 'SET title = :title',
    ReturnValues: "ALL_NEW",
    ExpressionAttributeValues: {
      ':title': serviceTitle
    }
  }, callback))
  .then(result => {
    //console.log(JSON.stringify(result));
    return result.Attributes
  });

  module.exports = { delService, addService, editService, getService };
