const f = require('./filters.js');

exports.deepfindbytag = function (providers,tags) {
  console.log("We are in");
  var providerlist = providers.reduce(function (pvlist, provider) {
    pvlist.push(f.findbytag(provider.services,tags));
    console.log("We are in providerlist");
    return pvlist;
  },[]);
  return providerlist[0];
}

exports.deepforeachbytag = function (providers,tagz,maxprice) {
  var o = [];
  providers.forEach(function(provider) {
    var provServices = f.findbytag(provider,tagz,maxprice);
    o = o.concat(provServices);
    return o;
  });

  return o;
}

exports.findbytag = function (provider,tags,maxprice) {
  var servicelist = provider.services.reduce(function (srvlist, service) {

    var tagsnum = service.tags.map(function (tag) {
        var cnt = 0;
        if (tags.includes(tag.title)) {
          cnt = cnt+1;
        }
        return cnt;
    });
  var offilter = offerfilter(service, maxprice);
  var offlength = offilter.length;
    if (tagsnum > 0 && offlength > 0) {
      service.offers = offilter;
      console.log(JSON.stringify(service));
      srvlist.push(service);
    }
    return srvlist;
  },[]);

  return servicelist;
};

function offerfilter(service,maxprice) {
  var offerlist = service.offers.reduce(function (offlist, offer) {
    if (offer.price < maxprice) {
      offlist.push(offer);
    }
    return offlist;
  },[]);

  return offerlist;
}
